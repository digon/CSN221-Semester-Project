# Cache Miss Pattern and Miss Predictability Analysis
Our topic for the semester project of CSN-221 (Computer Architecture and Microprocessors) is Cache Miss Pattern and Miss Predictability Analysis.

# Our Team
  
  -> [Gubbala Venkata Sai Kiran](https://github.com/saikiran-gubbala)                     [18114026 (CSE)]
  
  -> [Kovvuri Mani Sai Sampath Ammireddy](https://github.com/sampathkovvuri)            [18114041 (CSE)]
  
  -> [Pulla Maheshwar Reddy](https://github.com/mahesh12393)                              [18114063 (ECE)]
    
  -> [Agastya Varahala](https://github.com/agastya-v)                              [18116005 (ECE)]
  
  -> [Anmol Agarwal](https://github.com/BugsBunny0)                                 [18116013 (ECE)]
  
  -> [Anmol Thakur](https://github.com/anmol-thakur)                                  [18116014 (ECE)]
  
  -> [Taduvai Veera Venkata Naga Amaresh](https://github.com/amaresh203)            [18117109 (CSE)]

# Course Instructor
  [Sateesh Kumar Peddoju](https://github.com/drpskfec)
  
  [Associate Professor](https://www.iitr.ac.in/departments/CSE/pages/Home+Departments_and_Centres+Electronics_and_Computer_Engineering+People+Faculty+Peddoju__Sateesh_Kumar_.html), Department of CSE, IIT Roorkee 
