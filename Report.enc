%%%%%%%%%%%%  Generated using docx2latex.com  %%%%%%%%%%%%%%

%%%%%%%%%%%%  v2.0.0-beta  %%%%%%%%%%%%%%

\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{latexsym}
\usepackage{amsfonts}
\usepackage[normalem]{ulem}
\usepackage{array}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[backend=biber,
style=numeric,
sorting=none,
isbn=false,
doi=false,
url=false,
]{biblatex}\addbibresource{bibliography.bib}

\usepackage{subfig}
\usepackage{wrapfig}
\usepackage{wasysym}
\usepackage{enumitem}
\usepackage{adjustbox}
\usepackage{ragged2e}
\usepackage[svgnames,table]{xcolor}
\usepackage{tikz}
\usepackage{longtable}
\usepackage{changepage}
\usepackage{setspace}
\usepackage{hhline}
\usepackage{multicol}
\usepackage{tabto}
\usepackage{float}
\usepackage{multirow}
\usepackage{makecell}
\usepackage{fancyhdr}
\usepackage[toc,page]{appendix}
\usepackage[hidelinks]{hyperref}
\usetikzlibrary{shapes.symbols,shapes.geometric,shadows,arrows.meta}
\tikzset{>={Latex[width=1.5mm,length=2mm]}}
\usepackage{flowchart}\usepackage[paperheight=11.69in,paperwidth=8.27in,left=1.0in,right=1.0in,top=1.0in,bottom=1.0in,headheight=1in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\TabPositions{0.5in,1.0in,1.5in,2.0in,2.5in,3.0in,3.5in,4.0in,4.5in,5.0in,5.5in,6.0in,}

\urlstyle{same}


 %%%%%%%%%%%%  Set Depths for Sections  %%%%%%%%%%%%%%

% 1) Section
% 1.1) SubSection
% 1.1.1) SubSubSection
% 1.1.1.1) Paragraph
% 1.1.1.1.1) Subparagraph


\setcounter{tocdepth}{5}
\setcounter{secnumdepth}{5}


 %%%%%%%%%%%%  Set Depths for Nested Lists created by \begin{enumerate}  %%%%%%%%%%%%%%


\setlistdepth{9}
\renewlist{enumerate}{enumerate}{9}
		\setlist[enumerate,1]{label=\arabic*)}
		\setlist[enumerate,2]{label=\alph*)}
		\setlist[enumerate,3]{label=(\roman*)}
		\setlist[enumerate,4]{label=(\arabic*)}
		\setlist[enumerate,5]{label=(\Alph*)}
		\setlist[enumerate,6]{label=(\Roman*)}
		\setlist[enumerate,7]{label=\arabic*}
		\setlist[enumerate,8]{label=\alph*}
		\setlist[enumerate,9]{label=\roman*}

\renewlist{itemize}{itemize}{9}
		\setlist[itemize]{label=$\cdot$}
		\setlist[itemize,1]{label=\textbullet}
		\setlist[itemize,2]{label=$\circ$}
		\setlist[itemize,3]{label=$\ast$}
		\setlist[itemize,4]{label=$\dagger$}
		\setlist[itemize,5]{label=$\triangleright$}
		\setlist[itemize,6]{label=$\bigstar$}
		\setlist[itemize,7]{label=$\blacklozenge$}
		\setlist[itemize,8]{label=$\prime$}

\setlength{\topsep}{0pt}\setlength{\parskip}{8.04pt}
\setlength{\parindent}{0pt}

 %%%%%%%%%%%%  This sets linespacing (verticle gap between Lines) Default=1 %%%%%%%%%%%%%%


\renewcommand{\arraystretch}{1.3}


%%%%%%%%%%%%%%%%%%%% Document code starts here %%%%%%%%%%%%%%%%%%%%



\begin{document}
\begin{Center}
{\fontsize{15pt}{18.0pt}\selectfont Report\par}
\end{Center}\par

\begin{Center}
{\fontsize{15pt}{18.0pt}\selectfont Topic: Cache Miss Pattern and Miss Predictability Analysis\par}
\end{Center}\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textbf{Contributors:}\par}
\end{justify}\par

\setlength{\parskip}{12.0pt}
{\fontsize{15pt}{18.0pt}\selectfont -> Gubbala Venkata Sai Kiran [18114026 (CSE)]\par}\par

{\fontsize{15pt}{18.0pt}\selectfont -> Kovvuri Mani Sai Sampath Ammireddy [18114041 (CSE)]\par}\par

{\fontsize{15pt}{18.0pt}\selectfont -> \href{https://github.com/mahesh12393}{Pulla Maheshwar Reddy} [18116063 (ECE)]\par}\par

{\fontsize{15pt}{18.0pt}\selectfont -> \href{https://github.com/agastya-v}{Agastya Varahala} [18116005 (ECE)]\par}\par

{\fontsize{15pt}{18.0pt}\selectfont -> \href{https://github.com/BugsBunny0}{Anmol Agarwal} [18116013 (ECE)]\par}\par

{\fontsize{15pt}{18.0pt}\selectfont -> \href{https://github.com/anmol-thakur}{Anmol Thakur} [18116014 (ECE)]\par}\par

{\fontsize{15pt}{18.0pt}\selectfont -> Taduvai Veera Venkata Naga Amaresh [18117109 (CSE)]\par}\par


\vspace{\baselineskip}
\setlength{\parskip}{8.04pt}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textbf{Description of the topic:}\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont Our topic deals with the analysis of cache miss pattern and prediction of miss rate. Here the cache miss rate is defined as \textcolor[HTML]{222222}{the total cache misses divided by the total number of memory requests expressed as a percentage over a time interval. The proposed predictability model for cache miss rate is based on the LRU (Least recently used) and LFU (Least frequently used) cache implementations. Further expanding our model, we may use a locality pattern by forming reference groups to find out reuse distance which will further be used to find cache miss rate for a given input program.}\par}
\end{justify}\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textbf{\textcolor[HTML]{222222}{Why the topic is important:}}\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{222222}{The topic deals with the analysis of cache miss which is an important factor for determining the efficiency of a computer system. While studying the topic we learnt about different types of cache miss i.e. compulsory, capacity and conflict miss. We also studied different cache replacement policies i.e. FIFO, LIFO, LRU, LFU, MRU etc. We also studied about the two cache localities: temporal and spatial locality, which can be used to form a locality model of a given program. Thus, our knowledge regarding cache improved and we got an insight regarding the efficiency of a computer system.}\par}
\end{justify}\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textbf{Description of related work and methods used to implement this project:}\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont After going through all these information we came up with the thought of writing our programs for the basic cache replacement policies(LRU and LFU) because these are the most fundamental policies used in any other cache replacement policies and are the best options for any project in their initial phase.\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{222222}{First of all we tried to calculate miss rate using LRU (Least recently used) cache replacement policy. Discards the least recently used items first. This algorithm requires keeping track of what was used when, which is expensive if one wants to make sure the algorithm always discards the least recently used item. General implementations of this technique require keeping "age bits" for cache-lines and track the "Least Recently Used" cache-line based on age-bits. In such an implementation, every time a cache-line is used, the age of all other cache-lines changes.}\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{222222}{After this we tried to calculate miss rate using LFU (Least frequently used) cache replacement policy. In this method, system counts how often an item is required and then those items that are used least often are removed first. In this we store the value of how many times a block was accessed. When a new block is added in the cache, then the block which was used least number of times will be replaced. So of course while running an access sequence we will replace a block which was used least number of times from our cache. E.g., if X was used (accessed) 6 times and Y was used 4 times and others Z and W were used 8 times each, we will replace Y.}\par}
\end{justify}\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{222222}{After implementing LFU and LRU policies, we tried to understand the method of using reuse distance to calculate cache miss rate for a given program. It helped us in understanding cache behaviour for a given cache configuration (size, associativity, and line size) as the program data set varies. This method utilizes data reuse signature patterns, which are defined by measurements of a program's data reuse distance. Basically, the reuse distance is a measure of the capacity that a fully associative cache must have for the subsequent access to hit in the cache. In sequential execution, reuse distance is the number of unique data elements accessed between two consecutive references to the same element. Reuse distance is an important metric for analytical estimation of cache miss rate. To find the miss rate of a cache, the reuse distance profile has to be measured for that particular level and configuration of the cache. Significant amount of simulation time and overhead can be reduced if we can find the miss rate of higher-level cache. In this technique, reuse distance information from two input data sets of different sizes can be used to form a parameterized model which in turn can be used to calculate cache miss rates for any input size and for any size of fully associative cache. Also, the critical input data set sizes i.e. the knees in the miss rates, can be identified where the miss rate changes drastically for a given cache.}\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{333333}{By forming reference groups, one can model the program locality as predictable reuse distance patterns. We also employ a formula/function to calculate reuse distance i.e. d=c+e$\ast$ f(s), where f is a known function and s is the data size, c and e are unknown coefficients which can be found by assuming some type of predefined data fit. After this an overall locality model for the program can be formed by aggregation of the individual fitted functions for each reference group. Now we can estimate the capacity miss rate by using the fact that the groups with a reuse distance larger than or equal to the given cache size will be capacity misses. Also, the references with shorter reuse distance will have a hit in cache because of temporal locality. An estimation of the miss rate can be found from the number of these groups. The detailed algorithms for reuse distance prediction and cache miss rate estimation are given in the research papers. }\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{333333}{These algorithms may also be used to find out the maximum miss rate and the threshold input size. The threshold input size }Ts\textcolor[HTML]{333333}{ is the smallest input that generates the worst case hit ratio for a fully associative cache . If the predictive model only contains the constant pattern, then the cache miss rate is the same for all inputs. In this case, a meaningless null threshold input is generated.}\par}
\end{justify}\par

{\fontsize{15pt}{18.0pt}\selectfont  The fraction of compulsory misses is the ratio of the data touched to the total number of accesses. This ratio cannot be predicted, in general, because of data dependent features such as a variable number of iterations over the data. However, the relative fraction of compulsory misses is small when there is significant data reuse and can be ignored without significant impact on the miss rate.\par}\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textbf{\textcolor[HTML]{333333}{Graphical Analysis:}}\par}
\end{justify}\par

\setlength{\parskip}{9.96pt}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont \textcolor[HTML]{333333}{Using the third method of predictive model, one can plot graphs between data size and miss rates for a given cache configuration (size, associativity, and line size). }The graphs given below predict the miss rate of a fully associative cache with a given cache-block size. Each graph has one or more inputs in which the miss rate exhibits a sharp jump. These jumps occur at input data set sizes where another portion of the reuse histogram dependent on s moves beyond the cache size. At input data sizes that achieve the maximum miss rate, all hits in the cache are due to the portion of the reuse distance histogram that have a constant reuse distance within the cache size. The reuse distance pattern is insensitive to the total program size in GCC application.\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont We use the hit rate for the error measurement because it is more stable than the miss rate in regions where the miss rate is near zero. Miss rate can be improved by using the fully associative cache as it uses LRU policy which displaces blocks that will be used in the relatively near future because of insufficient capacity.\par}
\end{justify}\par


\vspace{\baselineskip}
\setlength{\parskip}{8.04pt}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 1 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=5.77in,height=8.54in]{./media/image1.GIF}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 1 Ends here %%%%%%%%%%%%%%%%%%%%

\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont Results:\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont Our own codes for LFU and LRU cache implementations:\par}
\end{justify}\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont LFU cache implementation code:\par}
\end{justify}\par



%%%%%%%%%%%%%%%%%%%% Figure/Image No: 2 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=3.99in,height=2.72in]{./media/image2.png}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 2 Ends here %%%%%%%%%%%%%%%%%%%%

\par



%%%%%%%%%%%%%%%%%%%% Figure/Image No: 3 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=3.98in,height=2.72in]{./media/image3.png}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 3 Ends here %%%%%%%%%%%%%%%%%%%%

\par



%%%%%%%%%%%%%%%%%%%% Figure/Image No: 4 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=3.99in,height=1.31in]{./media/image4.png}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 4 Ends here %%%%%%%%%%%%%%%%%%%%

\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont Output for test input:\par}
\end{justify}\par


\vspace{\baselineskip}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 5 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=6.27in,height=3.16in]{./media/image5.jpeg}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 5 Ends here %%%%%%%%%%%%%%%%%%%%

\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont LRU cache implementation code:\par}
\end{justify}\par


\vspace{\baselineskip}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 6 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=4.01in,height=2.73in]{./media/image6.png}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 6 Ends here %%%%%%%%%%%%%%%%%%%%

\par



%%%%%%%%%%%%%%%%%%%% Figure/Image No: 7 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=3.99in,height=2.31in]{./media/image7.png}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 7 Ends here %%%%%%%%%%%%%%%%%%%%

\par

\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont Output for test input:\par}
\end{justify}\par


\vspace{\baselineskip}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 8 starts here %%%%%%%%%%%%%%%%%%%%

\begin{figure}[H]
	\begin{Center}
		\includegraphics[width=6.27in,height=3.17in]{./media/image8.jpeg}
	\end{Center}
\end{figure}


%%%%%%%%%%%%%%%%%%%% Figure/Image No: 8 Ends here %%%%%%%%%%%%%%%%%%%%

\par


\vspace{\baselineskip}
\begin{justify}
{\fontsize{15pt}{18.0pt}\selectfont References:\par}
\end{justify}\par

\begin{justify}
\href{https://ieeexplore.ieee.org/abstract/document/1238004}{{\fontsize{15pt}{18.0pt}\selectfont https://ieeexplore.ieee.org/abstract/document/1238004}\par}
\end{justify}\par

\begin{justify}
\href{https://ieeexplore.ieee.org/document/4079516}{{\fontsize{15pt}{18.0pt}\selectfont https://ieeexplore.ieee.org/document/4079516}\par}
\end{justify}\par

\begin{justify}
\href{https://www.google.com/url?sa=t&source=web&rct=j&url=http://i.stanford.edu/pub/cstr/reports/csl/tr/96/707/CSL-TR-96-707.pdf&ved=2ahUKEwj3rsjQxMblAhVGO48KHaOnAXwQFjABegQIBxAB&usg=AOvVaw1QBUzNHXiHO7jLxrnfc0mI}{{\fontsize{15pt}{18.0pt}\selectfont https://www.google.com/url?sa=t$\&$ source=web$\&$ rct=j$\&$ url=http://i.stanford.edu/pub/cstr/reports/csl/tr/96/707/CSL-TR-96-707.pdf$\&$ ved=2ahUKEwj3rsjQxMblAhVGO48KHaOnAXwQFjABegQIBxAB$\&$ usg=AOvVaw1QBUzNHXiHO7jLxrnfc0mI}\par}
\end{justify}\par


\vspace{\baselineskip}

\vspace{\baselineskip}

\printbibliography
\end{document}