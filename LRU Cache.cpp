#include<bits/stdc++.h>
using namespace std;
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL); 
#define ll long long
#define int long long
#define ld long double
#define endl '\n'
#define vi vector<int>
#define vl vector<long long>
#define vpii vector<pii>
#define pb push_back
#define gcd __gcd
#define mp make_pair
#define pii pair<int,int>
#define pll pair<long long,long long>
#define pld pair<long double,long double>
#define mod 1000000007
#define rep(i,n) for (ll i = 0; i < n; i++)
#define repp(i,a,b) for(ll i = a ; i<b ; i++)
#define reppr(i,a,b) for(ll i = a-1 ; i>=b ; i--)
#define repr(i,n) for (ll i = n - 1; i >= 0; i--)
#define ff first
#define ss second
#define pc putchar_unlocked
#define gc getchar_unlocked
#define inf 1000000000000000000
#define pi 3.14159265358979323846
#define eps 0.0000000001

int32_t main()
{
	fast;
	int n, m;
	cin >> n >> m;
	int memory[n][n];
	int cache[m][n];
	map<int,int> key;
	vi order;
	int full = 0;
	int counter = 0;
	int miss = 0;
	rep(i,n)
		rep(j,n)
			cin >> memory[i][j];
	int q;
	cin >> q;
	int test = q;
	while(q--)
	{
		int i, j;
		cin >> i >> j;
		i-- , j-- ;
		if(key[i]!=0)
		{
			// cout << "T1" << endl;
			rep(k,order.size())
				if(order[k]==i)
					order.erase(order.begin()+k);
			order.push_back(i);
		}
		else if(key[i] == 0  && full == 1)
		{
			// cout << "T2" << endl;
			key[i] = key[order[0]];
			int tbc = key[order[0]]-1;
			key[order[0]] = 0;
			rep(k,n)
				cache[tbc][k] = memory[i][k];
			order.erase(order.begin());
			order.push_back(i);
			miss++;
		}
		else if(key[i] == 0)
		{
			// cout << "T3" << endl;
			rep(k,order.size())
				if(order[k]==i)
					order.erase(order.begin()+k);
			order.push_back(i);
			key[i] = counter+1;
			rep(k,n)
				cache[counter][k] = memory[i][k];
			counter++;
			if(counter == m)
				full = 1;
			miss++;
		}
	}	
	cout << "Memory Size : " << n*n  << endl << "Cache Size : "<< m*n << endl << "Miss Rate : " <<   (miss*1.0)/(test*1.0) << endl;
}
