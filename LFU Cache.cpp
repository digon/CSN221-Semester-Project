#include<bits/stdc++.h>
using namespace std;
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL); 
#define ll long long
#define int long long
#define ld long double
#define endl '\n'
#define vi vector<int>
#define vl vector<long long>
#define vpii vector<pii>
#define pb push_back
#define gcd __gcd
#define mp make_pair
#define pii pair<int,int>
#define pll pair<long long,long long>
#define pld pair<long double,long double>
#define mod 1000000007
#define rep(i,n) for (ll i = 0; i < n; i++)
#define repp(i,a,b) for(ll i = a ; i<b ; i++)
#define reppr(i,a,b) for(ll i = a-1 ; i>=b ; i--)
#define repr(i,n) for (ll i = n - 1; i >= 0; i--)
#define ff first
#define ss second
#define pc putchar_unlocked
#define gc getchar_unlocked
#define inf 1000000000000000000
#define pi 3.14159265358979323846
#define eps 0.0000000001

int32_t main()
{
	fast;
	int n, m;
	cin >> n >> m;
	int memory[n][n];
	int cache[m][n];
	map<int,int> key;
	map<int,set<int>> index;
	set<int> ss;
	vi order;
	int full = 0;
	int counters = 0;
	int miss = 0;
	int counter[n] = {};
	rep(i,n)
		rep(j,n)
			cin >> memory[i][j];
	int q;
	cin >> q;
	int test = q;
	while(q--)
	{
		int i, j;
		cin >> i >> j;
		i-- , j-- ;
		if(key[i]!=0)
		{
			// cout << "T1" << endl;
			index[counter[i]].erase(i);
			counter[i]++;
			index[counter[i]].insert(i);
		}
		else if(key[i] == 0  && full == 1)
		{
			// cout << "T2" << endl;
			int count = 0;
			for(auto k : ss)
			{
				count = k ;
				break;
			}
			int tbc = 0;
			for(auto k : index[count])
			{
				tbc = k;
				break;
			}
			int indd = tbc;
			tbc = key[indd];
			key[indd] = 0;
			key[i] = tbc;
			if(index[count].size()==1)
				ss.erase(count);
			if(index[count].size()>0)		
				index[count].erase(index[count].begin());
			counter[i]++;
			ss.insert(counter[i]);
			if(index[counter[i]-1].size()==1)
				ss.erase(counter[i]-1);
			if(counter[i]!=1)
				index[counter[i]-1].erase(i);
			index[counter[i]].insert(i);
			rep(k,n)
				cache[tbc][k] = memory[i][k];
			miss++;
		}
		else if(key[i] == 0)
		{
			// cout << "T3" << endl;
			ss.insert(1);
			counter[i] = 1;
			index[1].insert(i);
			key[i] = counters+1;
			rep(k,n)
				cache[counters][k] = memory[i][k];
			counters++;
			if(counters == m)
				full = 1;
			miss++;
		}
	}	
	cout << "Memory Size : " << n*n  << endl << "Cache Size : "<< m*n << endl << "Miss Rate : " <<   (miss*1.0)/(test*1.0) << endl;
}
